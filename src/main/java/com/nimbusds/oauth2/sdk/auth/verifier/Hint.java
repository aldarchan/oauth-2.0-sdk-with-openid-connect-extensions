package com.nimbusds.oauth2.sdk.auth.verifier;


/**
 * Hints intended for the client authentication verifier.
 */
public enum Hint {

	/**
	 * The client has a registered remote JWK set (jwks_uri).
	 */
	CLIENT_HAS_REMOTE_JWK_SET
}
